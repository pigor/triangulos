class V3::TriangulosController < ApplicationController
  def new
    @triangulo = V3::Triangulo.new

    if params[:triangulo]
      @mensagem = "Triângulo #{params[:triangulo]}"
      @tipo_triangulo = params[:triangulo] unless params[:triangulo] == "Inválido"
    end
  end

  def create
    triangulo = V3::Triangulo.calcular params
    triangulo ||= "Inválido"

    redirect_to v3_path(triangulo: triangulo)
  end
end
