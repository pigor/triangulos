class V1::TriangulosController < ApplicationController
  def new
    @triangulo = V1::Triangulo.new
    @mensagem = params[:mensagem]
    @mensagem ||= ""
  end

  def create
    mensagem = V1::Triangulo.calcular params

    redirect_to root_path(mensagem: mensagem)
  end
end
