class V1::Triangulo
  attr_accessor :lado1, :lado2, :lado3

  def self.calcular(triangulo={})
    lado1 = triangulo[:lado1]
    lado2 = triangulo[:lado2]
    lado3 = triangulo[:lado3]

    if lado1.to_i > 0 and lado2.to_i > 0 and lado3.to_i > 0
      return "Triângulo Equilatero" if lado1 == lado2 and lado2 == lado3
      return "Triângulo Isósceles" if lado1 == lado2 or lado2 == lado3 or lado1 == lado3
      return "Triângulo Escaleno" if lado1 != lado2 and lado2 != lado3 and lado1 != lado3
    end

    return ""
  end
end
