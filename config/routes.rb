Rails.application.routes.draw do
  root 'v1/triangulos#new'

  get '/v1', to: 'v1/triangulos#new'
  post :calcular, action: :create, controller: 'v1/triangulos'

  get '/v2', to: 'v2/triangulos#new'
  post :calcular2, action: :create, controller: 'v2/triangulos'

  get '/v3', to: 'v3/triangulos#new'
  post :calcular3, action: :create, controller: 'v3/triangulos'
end
