class V3::Triangulo
  attr_accessor :lado1, :lado2, :lado3

  def self.calcular(triangulo={})
    lado1 = triangulo[:lado1]
    lado2 = triangulo[:lado2]
    lado3 = triangulo[:lado3]

    if (lado1.to_i < lado2.to_i + lado3.to_i) && (lado2.to_i < lado1.to_i + lado3.to_i) && (lado3.to_i < lado1.to_i + lado2.to_i)
      return "Equilátero" if lado1 == lado2 and lado2 == lado3
      return "Escaleno" if lado1 != lado2 and lado2 != lado3 and lado1 != lado3

      return "Isósceles"
    end

    return nil
  end
end
