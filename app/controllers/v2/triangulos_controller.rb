class V2::TriangulosController < ApplicationController
  def new
    @triangulo = V2::Triangulo.new

    if params[:triangulo]
      @mensagem = "Triângulo #{params[:triangulo]}"
      @tipo_triangulo = params[:triangulo]
    end
  end

  def create
    triangulo = V2::Triangulo.calcular params

    redirect_to v2_path(triangulo: triangulo)
  end
end
